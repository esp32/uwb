//
// Created by moai8 on 15.07.2023.
//
#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "esp_timer.h"

#define GPIO_HANDSHAKE GPIO_NUM_2
#define GPIO_MOSI GPIO_NUM_11
#define GPIO_MISO GPIO_NUM_13
#define GPIO_SCLK GPIO_NUM_12
#define GPIO_CS GPIO_NUM_10

#define SENDER_HOST SPI2_HOST


//The semaphore indicating the slave is ready to receive stuff.
static QueueHandle_t rdySem; //创建一个队列量

/*
This ISR is called when the handshake line goes high.
*/
static void IRAM_ATTR gpio_handshake_isr_handler(void *arg) {
    //Sometimes due to interference or ringing or something, we get two irqs after eachother. This is solved by
    //looking at the time between interrupts and refusing any interrupt too close to another one.
    static uint32_t lasthandshaketime_us;
    uint32_t currtime_us = esp_timer_get_time();
    uint32_t diff = currtime_us - lasthandshaketime_us;
    if (diff < 1000) {
        return; //ignore everything <1ms after an earlier irq
    }
    lasthandshaketime_us = currtime_us;

    //Give the semaphore.
    BaseType_t mustYield = false;
    xSemaphoreGiveFromISR(rdySem, &mustYield); //give the semaphore
    if (mustYield) {
        portYIELD_FROM_ISR();
    }
}

//Main application
extern "C" void app_main(void) {
    esp_err_t ret;
    spi_device_handle_t handle;

    //Configuration for the SPI bus
    spi_bus_config_t buscfg = {
            .mosi_io_num=GPIO_MOSI,
            .miso_io_num=GPIO_MISO,
            .sclk_io_num=GPIO_SCLK,
            .quadwp_io_num=-1,
            .quadhd_io_num=-1
    };

    //Configuration for the SPI device on the other side of the bus
    spi_device_interface_config_t devcfg = {
            .command_bits=0,
            .address_bits=0,
            .dummy_bits=0,
            .mode=0,
            .duty_cycle_pos=128,        //50% duty cycle
            .cs_ena_posttrans=3,        //Keep the CS low 3 cycles after transaction, to stop slave from missing the last bit when CS has less propagation delay than CLK
            .clock_speed_hz=50000,
            .spics_io_num=GPIO_CS,
            .queue_size=3
    };

    //GPIO config for the handshake line.
    gpio_config_t io_conf = {
            .pin_bit_mask=(1 << GPIO_HANDSHAKE),
            .mode=GPIO_MODE_INPUT,
            .pull_up_en=GPIO_PULLUP_ENABLE,
            .intr_type=GPIO_INTR_POSEDGE,
    };

    int n = 0;
    char sendbuf[128] = {0};
    char recvbuf[128] = {0};
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));

    //Create the semaphore.
    rdySem = xSemaphoreCreateBinary(); //创建一个二进制新号量

    //Set up handshake line interrupt.
    gpio_config(&io_conf);
    gpio_install_isr_service(0);
    gpio_set_intr_type(GPIO_HANDSHAKE, GPIO_INTR_POSEDGE);
    gpio_isr_handler_add(GPIO_HANDSHAKE, gpio_handshake_isr_handler, NULL);

    //Initialize the SPI bus and add the device we want to send stuff to.
    ret = spi_bus_initialize(SENDER_HOST, &buscfg, SPI_DMA_CH_AUTO);
    assert(ret == ESP_OK);
    ret = spi_bus_add_device(SENDER_HOST, &devcfg, &handle);
    assert(ret == ESP_OK);

    //Assume the slave is ready for the first transmission: if the slave started up before us, we will not detect
    //positive edge on the handshake line.
    xSemaphoreGive(rdySem);

    while (1) {
        int res = snprintf(sendbuf, sizeof(sendbuf),
                           "Sender, transmission no. %04i. Last time, I received: \"%s\"", n, recvbuf);
        if (res >= sizeof(sendbuf)) {
            printf("Data truncated\n");
        }
        t.length = sizeof(sendbuf) * 8;
        t.tx_buffer = sendbuf;
        t.rx_buffer = recvbuf;
        //Wait for slave to be ready for next byte before sending
        xSemaphoreTake(rdySem, portMAX_DELAY); //Wait until slave is ready
        ret = spi_device_transmit(handle, &t);
        printf("Received: %s\n", recvbuf);
        n++;
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }

    //Never reached.
    ret = spi_bus_remove_device(handle);
    assert(ret == ESP_OK);
}
