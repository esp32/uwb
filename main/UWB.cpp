#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <cstddef>
#include <string.h>
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "esp_timer.h"

#define GPIO_HANDSHAKE GPIO_NUM_2
#define GPIO_MOSI GPIO_NUM_11
#define GPIO_MISO GPIO_NUM_13
#define GPIO_SCLK GPIO_NUM_12
#define GPIO_CS GPIO_NUM_10

#define SENDER_HOST SPI2_HOST
typedef enum {
    LOW = 0,
    HIGH = 1,
} Pin_Level;

// SPI Modes
constexpr uint16_t SPI_RD_BIT =     0x0000U;
constexpr uint16_t SPI_WR_BIT =     0x8000U;
constexpr uint16_t SPI_AND_OR_8 =   0x8001U;
constexpr uint16_t SPI_AND_OR_16 =  0x8002U;
constexpr uint16_t SPI_AND_OR_32 =  0x8003U;

class DW3000{
    public:
    gpio_num_t SPI_MOSI;
    gpio_num_t SPI_MISO;
    gpio_num_t SPI_SCLK;
    gpio_num_t SPI_CS;
    gpio_num_t DW_Reset;
    spi_transaction_t message;

    // SPI interface
    esp_err_t ret;
    spi_device_handle_t DW3000_SPI_Handle;


    void SPI_Init(void){
        this->SPI_MOSI = GPIO_NUM_11;
        this->SPI_MISO = GPIO_NUM_13;
        this->SPI_SCLK = GPIO_NUM_12;
        this->SPI_CS = GPIO_NUM_10;
        this->DW_Reset = GPIO_NUM_9;


        gpio_reset_pin(SPI_CS);
        gpio_set_direction(SPI_CS,GPIO_MODE_OUTPUT);
        gpio_reset_pin(DW_Reset);
        gpio_set_direction(DW_Reset,GPIO_MODE_OUTPUT_OD);




        //Configuration for the SPI bus
        spi_bus_config_t buscfg = {
                .mosi_io_num=this->SPI_MOSI,
                .miso_io_num=this->SPI_MISO,
                .sclk_io_num=this->SPI_SCLK,
                .quadwp_io_num=-1,
                .quadhd_io_num=-1
        };

        //Configuration for the SPI device on the other side of the bus
        spi_device_interface_config_t DW3000_SPI_Config = {
                .command_bits=0,
                .address_bits=0,
                .dummy_bits=0,
                .mode=0,
                .duty_cycle_pos=128,        //50% duty cycle
                .cs_ena_posttrans=3,        //Keep the CS low 3 cycles after transaction, to stop slave from missing the last bit when CS has less propagation delay than CLK
                .clock_speed_hz = SPI_MASTER_FREQ_26M,
                .spics_io_num=this->SPI_CS,
                .queue_size=3
        };
        //Initialize the SPI bus
        ret = spi_bus_initialize(SENDER_HOST, &buscfg, SPI_DMA_CH_AUTO);
        assert(ret == ESP_OK);
        //Add the device we want to send stuff to.
        ret = spi_bus_add_device(SENDER_HOST, &DW3000_SPI_Config, &DW3000_SPI_Handle);
        assert(ret == ESP_OK);
    };

    void write_to_spi(uint8_t address, uint16_t offset, uint16_t length, uint8_t* buffer, uint16_t mode){
        uint8_t header[2];
        int headerLength = 0;

        uint16_t addr = (address << 9) | (offset << 2);

        header[0] = (uint8_t)((mode | addr) >> 8);
        header[1] = (uint8_t)(addr | (mode & 0x03));

        if (length == 0) {
            header[0] = (uint8_t)(address << 1) | 0x81;
            headerLength = 1;
        } else if (offset == 0 && (mode == SPI_WR_BIT || mode == SPI_RD_BIT)) {
            headerLength = 1;
        } else {
            header[0] |= 0x40;
            headerLength = 2;
        }

        SPI.beginTransaction(_SPISettings);
        digitalWrite(csn, LOW);

        for (int i = 0; i < headerLength; i++) {
            SPI.transfer(header[i]);
        }

        switch (mode) {
            case SPI_AND_OR_8:
            case SPI_AND_OR_16:
            case SPI_AND_OR_32:
            case SPI_WR_BIT: {
                for (int i = 0; i < length; i++) {
                    SPI.transfer(buffer[i]);
                }
                break;
            }
            case SPI_RD_BIT: {
                for (int i = 0; i < length; i++) {
                    buffer[i] = SPI.transfer(0x00);
                }
                break;
            }
        }

        delayMicroseconds(5);
        digitalWrite(csn, HIGH);
        SPI.endTransaction();

    };


    void initialization(void){
        this->reset();
        delay_ms(2);



    };

    void delay_ms(int ms){
        vTaskDelay(ms/ portTICK_PERIOD_MS);
    };

    void reset(void){
        gpio_set_level(this->DW_Reset, LOW);
        delay_ms(2);
        gpio_set_level(this->DW_Reset, HIGH);
        delay_ms(5);

    };
};


//Main application
extern "C" void app_main(void) {


    int n = 0;
    char sendbuf[128] = {0};
    char recvbuf[128] = {0};
    spi_transaction_t t;
    memset(&t, 0, sizeof(t));







    while (1) {
        int res = snprintf(sendbuf, sizeof(sendbuf),
                           "Sender, transmission no. %04i. Last time, I received: \"%s\"", n, recvbuf);
        if (res >= sizeof(sendbuf)) {
            printf("Data truncated\n");
        }
        t.length = sizeof(sendbuf) * 8;
        t.tx_buffer = sendbuf;
        t.rx_buffer = recvbuf;
        //Wait for slave to be ready for next byte before sending
        ret = spi_device_transmit(DW3000_SPI_Handle, &t);

        vTaskDelay(500 / portTICK_PERIOD_MS);
    }

    //Never reached.
    ret = spi_bus_remove_device(DW3000_SPI_Handle);
    assert(ret == ESP_OK);
}
